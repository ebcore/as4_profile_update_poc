
from lxml import etree
from zipfile import ZipFile
import re, logging, json

nsmap = {
    'table': 'urn:oasis:names:tc:opendocument:xmlns:table:1.0',
    'text': 'urn:oasis:names:tc:opendocument:xmlns:text:1.0'
}

class ServiceModel():

    def __init__(self):
        self.edigas4_party_roles = {}
        self.edigas5_party_roles = {}
        self.edigas6_party_roles = {}
        self.service_areas = {}
        self.service_edigas_version = {}
        self.document_schemas = {}
        self.document_names_v4 = {}
        self.document_names_v5 = {}
        self.document_names_v6 = {}
        self.document_schemas_v4 = {}
        self.document_schemas_v5 = {}
        self.document_schemas_v6 = {}
        self.rolemapping = {}
        self.easy_connect_export = []
        self.name = ''
        self.as4_services = (
            'http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/service',
            'http://docs.oasis-open.org/ebcore/ns/CertificateUpdate/v1.0',
            'http://docs.oasis-open.org/ebxml-msg/as4/200902/service'
        )
        self.as4_actions = (
            'http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/test',
            'UpdateCertificate',
            'ConfirmCertificateUpdate',
            'RejectCertificateUpdate'
        )
        self.as4_roles = (
            'http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator',
            'http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder'
        )

    def load(self, filepath):
        self.name = filepath
        print('Processing ODS file {}'.format(filepath))
        odf_zip = ZipFile(filepath)
        odf_file = odf_zip.open('content.xml')
        odf_infoset = etree.parse(odf_file)
        del(odf_zip)

        self.load_dictionary(
            self.service_areas,
            odf_infoset.xpath(
                '//table:table[@table:name="Service_Process_Areas"]',
                namespaces=nsmap)[0],
            1,
            2,
            'Service process areas'
        )

        self.load_dictionary(
            self.service_edigas_version,
            odf_infoset.xpath(
                '//table:table[@table:name="Service_Process_Areas"]',
                namespaces=nsmap)[0],
            1,
            3,
            'Service related EDIGAS version'
        )

        """
        EDIGAS 4
        """

        self.load_dictionary(
            self.edigas4_party_roles,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS4_RoleType"]',
                namespaces=nsmap)[0],
            1,
            2,
            'EDIGAS 4 party roles'

        )

        self.load_dictionary(
            self.document_names_v4,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS4_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            2,
            'EDIGAS 4 Standard document types'
        )
        self.load_dictionary(
            self.document_schemas_v4,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS4_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            3,
            'EDIGAS 4 standard document type-schema'
        )

        self.document_names_v4_used = {}

        """
        EDIGAS 5
        """

        self.load_dictionary(
            self.edigas5_party_roles,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_5_RoleType"]',
                namespaces=nsmap)[0],
            1,
            2,
            'EDIGAS 5 party roles'

        )

        self.load_dictionary(
            self.document_names_v5,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_5_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            2,
            'EDIGAS 5 Standard document types'
        )

        self.load_dictionary(
            self.document_schemas_v5,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_5_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            3,
            'EDIGAS 5 standard document type-schema'
        )

        self.document_names_v5_used = {}

        """
        EDIGAS 6
        """
        self.load_dictionary(
            self.edigas6_party_roles,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_6_RoleType"]',
                namespaces=nsmap)[0],
            1,
            3,
            'EDIGAS 6 party roles'

        )

        self.load_dictionary(
            self.document_names_v6,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_6_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            2,
            'EDIGAS 6 Standard document types'
        )

        self.load_dictionary(
            self.document_schemas_v6,
            odf_infoset.xpath(
                '//table:table[@table:name="EDIGAS_6_StandardDocumentType"]',
                namespaces=nsmap)[0],
            1,
            3,
            'EDIGAS 6 standard document type-schema'
        )

        self.document_names_v6_used = {}

        self.load_exchanges(
            odf_infoset.xpath(
                '//table:table[@table:name="Message_Exchanges"]',
                namespaces=nsmap)[0]
        )

        for (version,  dnames, docnames_used) in [
            (4, self.document_schemas_v4, self.document_names_v4_used),
            (5, self.document_schemas_v5, self.document_names_v5_used),
            (6, self.document_schemas_v6, self.document_names_v6_used)
        ]:
            for i in dnames:
                if i in docnames_used:
                    pass
                    #print('Version {} code {} is used'.format(version, i))
                else:
                    print('Version {} code {} is NOT used'.format(version, i))

    def load_dictionary(self, dictionary, table, key_column, value_column, dictname=''):
        for counter, row in enumerate(
            table.xpath('child::table:table-row[position()>1]',
                namespaces=nsmap)[0:],
            start=2
        ):
            cells = row.xpath('child::table:table-cell',
                              namespaces=nsmap)

            if cellcontent(cells, key_column) is None:
                #print('Skipping None valued key {} {}'.format(cellcontent(cells, key_column), dictname))
                pass
            else:
                key = cellcontent(cells, key_column)
                value = cellcontent(cells, value_column)
                if key in dictionary:
                    print('Warn: duplicate key in dictionary {}: {} {} {}'.format(dictname,
                                                                                  key,
                                                                                  dictionary[key],
                                                                                  value
                                                                               ))
                dictionary[key] = value

    def load_exchanges(self, exchange_table):
        for counter, row in enumerate(
            exchange_table.xpath('child::table:table-row[position()>1]',
                namespaces=nsmap),  # skip header row
            start=2
        ):
            try:
                cells = row.xpath('child::table:table-cell',
                                  namespaces=nsmap)
                if len(cells) > 4:
                    area = cellcontent(cells, 1)
                    service = cellcontent(cells, 2)
                    action = cellcontent(cells, 3)
                    fromrolecodes = cellcontent(cells, 4)
                    torolecodes = cellcontent(cells, 6)
                    doccode =  cellcontent(cells, 8)
                    entsog_defined = cellcontent(cells, 9)
                    #docschema = cellcontent(cells, 11)

                    if service is None or action is None:
                        pass
                    else:
                        if service not in self.service_edigas_version and service not in self.as4_services:
                            print('Row {} EDIGAS version unknown for: {}'.format(counter, service))
                        else:
                            edigas_version = self.service_edigas_version.get(service)
                            if service not in self.service_areas and service not in self.as4_services:
                                print('Row {} service area unknown for: {}'.format(counter, service))
                            else:
                                servicearea = self.service_areas.get(service)
                                role_lookup_table = docnames_lookup_table = docschemas_lookup_table = {}
                                if edigas_version == "4":
                                    role_lookup_table = self.edigas4_party_roles
                                    docnames_lookup_table = self.document_names_v4
                                    docschemas_lookup_table = self.document_schemas_v4
                                    docnames_used = self.document_names_v4_used
                                elif edigas_version == "5":
                                    role_lookup_table = self.edigas5_party_roles
                                    docnames_lookup_table = self.document_names_v5
                                    docschemas_lookup_table = self.document_schemas_v5
                                    docnames_used = self.document_names_v5_used
                                elif edigas_version == "6":
                                    role_lookup_table = self.edigas6_party_roles
                                    docnames_lookup_table = self.document_names_v6
                                    docschemas_lookup_table = self.document_schemas_v6
                                    docnames_used = self.document_names_v6_used

                                docvalue = docnames_lookup_table.get(doccode)

                                if doccode is not None:
                                    docnames_used[doccode] = doccode
                                    if doccode in docschemas_lookup_table:
                                        expected_schema = docschemas_lookup_table[doccode]
                                        if expected_schema is not None:
                                            for fromrolecode in re.split('\;', fromrolecodes):
                                                fromrolecode = fromrolecode.strip()
                                                for torolecode in re.split('\;', torolecodes):
                                                    torolecode = torolecode.strip()
                                                    self.process_exchange(service, action,
                                                                          fromrolecode, torolecode,
                                                                          doccode, entsog_defined,
                                                                          expected_schema, counter,
                                                                          edigas_version, servicearea,
                                                                          role_lookup_table,
                                                                          docvalue)
                                    else:
                                        print(
                                            'Row {} {} {} {} unknown EDIGAS v{} Part Document Code: {}'.format(
                                                counter,
                                                service,
                                                fromrolecode,
                                                torolecode,
                                                edigas_version,
                                                doccode)
                                        )
                                else:
                                    self.process_exchange(service, action,
                                                          fromrolecodes, torolecodes,
                                                          None, entsog_defined,
                                                          None, counter,
                                                          None, None,
                                                          {},
                                                          None)
            except:
                print(etree.tostring(row))
                raise

    def process_exchange(self, service, action, fromrolecode, torolecode, doccode, entsog_defined,
                         docschema, counter, edigas_version, area, role_lookup_table, docvalue):
        di = {}
        for (label, var) in [
            ('edigasversion', edigas_version),
            ('servicecode', service),
            ('action', action),
            ('documentcode', doccode),
            ('documentvalue', docvalue),
            ('documentschema', docschema),
            ('area', area),
            ('entsog_defined', from_boolean(entsog_defined))
        ]:
            if var is not None:
                di[label] = var

        fromrolecode, torolecode = fromrolecode.strip(), torolecode.strip()

        if fromrolecode is not None:
            di['fromrolecode'] = fromrolecode
            if fromrolecode not in role_lookup_table and fromrolecode not in self.as4_roles:
                print('Row {} {} {} {} unknown EDIGAS {} From Role Code: \'{}\''.format(counter, service,
                                                                                     fromrolecode, torolecode,
                                                                                     edigas_version, fromrolecode))
            else:
                if fromrolecode in role_lookup_table:
                    di['fromrole'] = role_lookup_table.get(fromrolecode)
                if torolecode is not None:
                    di['torolecode'] = torolecode
                    if torolecode not in role_lookup_table and torolecode not in self.as4_roles:
                        print('Row {} {} {} {} unknown EDIGAS {} To Role Code: \'{}\''.format(counter, service,
                                                                                    fromrolecode, torolecode,
                                                                                    edigas_version, torolecode))

                    elif torolecode in role_lookup_table:
                        di['torole'] = role_lookup_table.get(torolecode)
        self.easy_connect_export.append(di)

        if len(fromrolecode) > 2:
            if fromrolecode not in self.rolemapping:
                self.rolemapping[fromrolecode] = {}
            if torolecode not in self.rolemapping[fromrolecode]:
                self.rolemapping[fromrolecode][torolecode] = {}
            if service not in self.rolemapping[fromrolecode][torolecode]:
                self.rolemapping[fromrolecode][torolecode][service] = {'send': {},
                                                                       'receive': {}}
            if action not in self.rolemapping[fromrolecode][torolecode][service]['send']:
                self.rolemapping[fromrolecode][torolecode][service]['send'][action] = []

            if doccode not in self.rolemapping[fromrolecode][torolecode][service]['send'][action]:
                self.rolemapping[fromrolecode][torolecode][service]['send'][action].append(doccode)
                logging.debug('{} {} {} {} send {}'.format(fromrolecode, torolecode,
                                                           service, action, doccode))

            if torolecode not in self.rolemapping:
                self.rolemapping[torolecode] = {}
            if fromrolecode not in self.rolemapping[torolecode]:
                self.rolemapping[torolecode][fromrolecode] = {}
            if service not in self.rolemapping[torolecode][fromrolecode]:
                self.rolemapping[torolecode][fromrolecode][service] = {'send': {},
                                                                       'receive': {}}
            if action not in self.rolemapping[torolecode][fromrolecode][service]['receive']:
                self.rolemapping[torolecode][fromrolecode][service]['receive'][action] = []
            if doccode not in self.rolemapping[torolecode][fromrolecode][service]['receive'][action]:
                self.rolemapping[torolecode][fromrolecode][service]['receive'][action].append(doccode)
                logging.debug('{} {} {} {} receive {}'.format(fromrolecode, torolecode,
                                                              service, action, doccode))

    def export_easee_connect(self, fn=None):
        if fn is None:
            fn = self.name+'.json'
        with open(fn, 'w', encoding='utf-8') as f:
            json.dump(self.easy_connect_export, f, ensure_ascii=False, indent=4)

def cellcontent(cells, index):
    counter2 = 1
    for cell in cells:
        if counter2 == index:
            return text_or_none(cell)
        else:
            repeated_cells = cell.get(
                '{urn:oasis:names:tc:opendocument:xmlns:table:1.0}number-columns-repeated'
            )
            if repeated_cells is not None:
                if counter2+int(repeated_cells) > index:
                    return text_or_none(cell)
                else:
                    counter2 += int(repeated_cells)
            else:
                counter2 += 1

def text_or_none(cell):
    p_list = cell.xpath('child::text:p', namespaces=nsmap)
    if len(p_list) > 0:
        text_content = ''.join(cell.xpath('descendant::text()'))
        return text_content.strip()
    else:
        return None

def from_boolean(value):
    if value in ('n', 'N', '0'):
        return False
    elif value in ('y', 'Y', '1'):
        return True
    else:
        return value


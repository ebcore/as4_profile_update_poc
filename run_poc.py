import json, logging
import lxml.etree
from os.path import abspath, dirname, join, split, splitext
from os import listdir
from inspect import getsourcefile

from service_model import ServiceModel
from cppa3_profile import export_profile

from cppa3.unify import unify, UnificationException
from cppa3.profile import ChannelProfileHandler

model_sheet = 'model/INT0698_210604_AS4-mapping-table_Edigas_4_7-3.ods'
parties_db = 'parties.json'

easee_connect_export = 'easee_connect/INT0698_210604_AS4-mapping-table_Edigas_4_7-3.json'

if __name__ == '__main__':
    """ 
    Load the service action table from the ODF sheet.
    """
    m = ServiceModel()
    m.load(model_sheet)

    """
    The following method exports the model in a (redundant) format used by the EASEE-connect service.
    For information,  see https://easee-gas.eu/easee-connect. 
    """
    m.export_easee_connect(easee_connect_export)

    """
    From the information in the "parties" JSON databse, we will generate CPPA3 profile documents.  
    Using these profiles, we will generate agreement documents.
    This is the definition of where those documents will be stored.
    """
    thisdir = dirname(abspath(getsourcefile(lambda: 0)))
    profile_path = join(thisdir, 'cppa3_profiles')
    agreements_path = join(thisdir, 'cppa3_agreements')

    """
    We use a simplified representations of ebMS3 channel definitions of the AS4 profile versions.
    They are stored in an XML file under "as4_profiles" and loaded in an instance of the channel profile
    handler class defined in the PyPi cppa3 package. 
    """
    defaults_path = join(thisdir, 'as4_profiles')
    defaults_definitions = join(defaults_path, 'channel_defaults.xml')
    parser = lxml.etree.XMLParser(remove_blank_text=True)
    channel_defaults = lxml.etree.parse(defaults_definitions, parser)
    profilehandler = ChannelProfileHandler(channel_defaults)

    """
    Using the information in the parties database and the defined defaults, we create profiles in 
    CPPA3 profile format from the simplified input data structure.
    """
    with open(parties_db, 'r', encoding='utf-8') as f:
        party_definitions = json.load(f)
        for party_def in party_definitions:
            with open(join(profile_path, party_def['eic_code']+'.cpp.xml'), 'wb') as f:
                cpp_base = export_profile(party_def, m.rolemapping)
                cpp_out = profilehandler.apply_profile_configs(cpp_base)
                f.write(lxml.etree.tostring(cpp_out, encoding="utf-8", pretty_print=True))

    """
    Now we check if the party profiles are compatible by using the "unify" function of the CPPA3
    library.  Compatibility depends on version of EDIG@S and of the AS4 profile. 
    So we create pairwise agreements in CPPA3 agreement format from the presented profiles. 
    """
    profiles = sorted(listdir(profile_path))
    last = len(profiles) - 1
    for c, file in enumerate(profiles, start=0):
        next = c+1
        for nextprofile in profiles[next:]:
            id1 = file[0:-8]
            id2 = nextprofile[0:-8]
            filepath = join(profile_path,file)
            nextfilepath = join(profile_path, nextprofile)
            (path, localpart) = split( filepath )
            (cpafilebase, fileext)= splitext( file )
            if fileext.lower() not in ('.xml'):
                logging.info('Skipping file that does not have a CPA extension: {0}'.format(file) )
            else:
                acpp =  lxml.etree.parse(filepath)
                bcpp =  lxml.etree.parse(nextfilepath)
            try:
                cpa = unify(acpp.getroot(), bcpp.getroot(), remove_unused_certs=True)
                logging.info('Successfully unified {} {}'.format(id1, id2))
                cpa_file= id1+'_'+id2+'.cpa.xml'
                fd = open(join(agreements_path, cpa_file), 'wb')
                fd.write(lxml.etree.tostring(cpa, pretty_print=True))
                fd.close()
            except UnificationException as e:
                logging.error('Unable to unify {} {}'.format(id1, id2))

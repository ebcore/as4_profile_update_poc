
import lxml.etree, hashlib, base64, datetime, logging, re

NSMAP = {'cppa': 'http://docs.oasis-open.org/ebcore/ns/cppa/v3.0',
         'ds': 'http://www.w3.org/2000/09/xmldsig#',
         'xml': 'http://www.w3.org/XML/1998/namespace'}

def ds_ns(el):
    return '{{{}}}{}'.format(NSMAP['ds'],el)

def xml_ns(el):
    return '{{{}}}{}'.format(NSMAP['xml'],el)

def cppa_ns(el):
    return '{{{}}}{}'.format(NSMAP['cppa'],el)

def genid(value):
    m = hashlib.sha224()
    m.update(value.encode('utf-8'))
    return str('_')+str(base64.b32encode(m.digest())[:6].decode('utf-8'))


def export_profile(party_def, rolemapping):
    cpp = lxml.etree.Element(cppa_ns('CPP'), nsmap = NSMAP)
    _attach_profileinfo(cpp, party_def['eic_code'])
    _attach_partyinfo(cpp, party_def['eic_code'], party_def['name'],
                      party_def.get('rsa_signing_cert'),
                      party_def.get('eddsa_signing_cert'))
    payload_profiles = _attach_service_specifications(cpp, party_def['roles'], rolemapping,
                                                      party_def['services'], party_def.get('profiles'))
    _attach_channelinfo(cpp, party_def['endpoint'], party_def.get('profiles'),
                        party_def.get('rsa_signing_cert'),
                        party_def.get('eddsa_signing_cert'))
    _attach_payload_profile(cpp, payload_profiles)
    return cpp

def _attach_profileinfo(cpp, partycode):
    prinfo = lxml.etree.SubElement(cpp, cppa_ns('ProfileInfo'))
    prinid = lxml.etree.SubElement(prinfo, cppa_ns('ProfileIdentifier'))
    prinid.text = '{}_{}'.format(partycode,datetime.datetime.isoformat(datetime.datetime.now()))

def _attach_partyinfo(cpp, partycode, partyname,
                      rsa_signing_certificate=None,
                      eddsa_signing_certificate=None):
    pinfo = lxml.etree.SubElement(cpp, cppa_ns('PartyInfo'))
    pname = lxml.etree.SubElement(pinfo, cppa_ns('PartyName'))
    pname.set(xml_ns('lang'), 'en')
    pname.text = partyname
    pcode = lxml.etree.SubElement(pinfo, cppa_ns('PartyId'),
                                  type='http://www.entsoe.eu/eic-codes/eic-party-codes-x')
    pcode.text = partycode
    if rsa_signing_certificate is not None:
        scert = lxml.etree.SubElement(pinfo, cppa_ns('Certificate'), id=genid(rsa_signing_certificate))
        keyinfo = lxml.etree.SubElement(scert, ds_ns('KeyInfo'))
        keyname = lxml.etree.SubElement(keyinfo, ds_ns('KeyName'))
        keyname.text = 'RSA signing certificate for {}'.format(partyname)
        x509data = lxml.etree.SubElement(keyinfo, ds_ns('X509Data'))
        x509cert = lxml.etree.SubElement(x509data, ds_ns('X509Certificate'))
        x509cert.append(lxml.etree.Comment(rsa_signing_certificate))

    if eddsa_signing_certificate is not None:
        enccert = lxml.etree.SubElement(pinfo, cppa_ns('Certificate'), id=genid(eddsa_signing_certificate))
        keyinfo = lxml.etree.SubElement(enccert, ds_ns('KeyInfo'))
        keyname = lxml.etree.SubElement(keyinfo, ds_ns('KeyName'))
        keyname.text = 'EDDSA signing certificate for {}'.format(partyname)
        x509data = lxml.etree.SubElement(keyinfo, ds_ns('X509Data'))
        x509cert = lxml.etree.SubElement(x509data, ds_ns('X509Certificate'))
        x509cert.append(lxml.etree.Comment(eddsa_signing_certificate))
    """
    certdefel = lxml.etree.SubElement(pinfo, cppa('CertificateDefaults'))
    certref = lxml.etree.SubElement(certdefel, cppa('SigningCertificateRef'),
                                    certId=genid('sign_'+partycode))

    certref = lxml.etree.SubElement(certdefel, cppa('EncryptionCertificateRef'),
                                    certId=genid('enc_'+partycode))
    """

def _attach_service_specifications(cpp, roles, rolemapping, services, profiles):
    counter1 = 0
    counter2 = 0
    payload_profiles = {}
    for partyrole in roles:
        if partyrole not in rolemapping:
            pass
        else:
            for counterpartyrole in rolemapping[partyrole]:
                service_specification = lxml.etree.Element(cppa_ns('ServiceSpecification'))
                partyroleel = lxml.etree.SubElement(service_specification, cppa_ns('PartyRole'),
                                                    name=partyrole)
                counterpartyroleel = lxml.etree.SubElement(service_specification, cppa_ns('CounterPartyRole'),
                                                           name= counterpartyrole)
                services_count = 0
                for service in rolemapping[partyrole][counterpartyrole]:
                    if service not in services:
                        logging.info('Skipping unsupported service {}'.format(service))
                    else:
                        services_count += 1
                        binding = lxml.etree.SubElement(service_specification, cppa_ns('ServiceBinding'))
                        serviceel = lxml.etree.SubElement(binding, cppa_ns('Service'))
                        if re.match('http:', service) == None:
                            serviceel.set('type', 'http://edigas.org/service')

                        serviceel.text = service
                        counter1 += 1
                        counter2 = 0
                        for action in rolemapping[partyrole][counterpartyrole][service]['send']:
                            documenttypes = rolemapping[partyrole][counterpartyrole][service]['send'][action]
                            for documenttype in documenttypes:
                                counter2 +=1
                                actionel = lxml.etree.SubElement(binding, cppa_ns('ActionBinding'),
                                                                 sendOrReceive='send')
                                actionel.set('action', action)
                                actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                for profile in profiles:
                                    chref = lxml.etree.SubElement(actionel, cppa_ns('ChannelId'))
                                    chref.text = "ch_{}_send".format(profile)
                                if documenttype is not None:
                                    ppref = lxml.etree.SubElement(actionel, cppa_ns('PayloadProfileId'))
                                    ppref.text = 'pp_{}'.format(documenttype)
                                    payload_profiles['pp_{}'.format(documenttype)] = documenttype
                        for action in rolemapping[partyrole][counterpartyrole][service]['receive']:
                            documenttypes = rolemapping[partyrole][counterpartyrole][service]['receive'][action]
                            counter2 +=1
                            for documenttype in documenttypes:
                                counter2 +=1
                                actionel = lxml.etree.SubElement(binding, cppa_ns('ActionBinding'),
                                                                 sendOrReceive='receive')
                                actionel.set('action', action)
                                actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                for profile in profiles:
                                    chref = lxml.etree.SubElement(actionel, cppa_ns('ChannelId'))
                                    chref.text = "ch_{}_receive".format(profile)
                                if documenttype is not None:
                                    ppref = lxml.etree.SubElement(actionel, cppa_ns('PayloadProfileId'))
                                    ppref.text = 'pp_{}'.format(documenttype)
                                    payload_profiles['pp_{}'.format(documenttype)] = documenttype

                if services_count > 0:
                    cpp.append(service_specification)
    logging.debug(payload_profiles)
    return payload_profiles

def _attach_channelinfo(cpp, server_address, profiles=[],
                        rsa_signing_certificate=None,
                        eddsa_signing_certificate=None):
    for profile in profiles:
        channel = "ch_{}".format(profile)
        nch1 = lxml.etree.SubElement(cpp, cppa_ns('ebMS3Channel'), id='{}_send'.format(channel), transport='tr_send')
        cn = lxml.etree.SubElement(nch1, cppa_ns('ChannelProfile'))
        if profile == "3_6":
            cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v3/UserMessageChannel'
            if rsa_signing_certificate is not None:
                wss = lxml.etree.SubElement(nch1, cppa_ns('WSSecurityBinding'))
                signature = lxml.etree.SubElement(wss, cppa_ns('Signature'))
                certref = lxml.etree.SubElement(signature, cppa_ns('SigningCertificateRef'))
                certref.set('certId', genid(rsa_signing_certificate))

        elif profile == "4_0":
            cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v4/UserMessageChannel'
            if eddsa_signing_certificate is not None:
                wss = lxml.etree.SubElement(nch1, cppa_ns('WSSecurityBinding'))
                signature = lxml.etree.SubElement(wss, cppa_ns('Signature'))
                certref = lxml.etree.SubElement(signature, cppa_ns('SigningCertificateRef'))
                certref.set('certId', genid(eddsa_signing_certificate))

        nch2 = lxml.etree.SubElement(cpp, cppa_ns('ebMS3Channel'), id='{}_receive'.format(channel), transport='tr_receive')
        cn = lxml.etree.SubElement(nch2, cppa_ns('ChannelProfile'))
        if profile == "3_6":
            cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v3/UserMessageChannel'
        elif profile == "4_0":
            cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v4/UserMessageChannel'

    tr1 = lxml.etree.SubElement(cpp, cppa_ns('HTTPTransport'), id='tr_send')

    tr2 = lxml.etree.SubElement(cpp, cppa_ns('HTTPTransport'), id='tr_receive')
    addr = lxml.etree.SubElement(tr2, cppa_ns('Endpoint'))
    addr.text = server_address

def _attach_payload_profile(cpp, payload_profiles):
    ppiterator = payload_profiles.items()
    for pp_id, pp_document_code in ppiterator:
        if pp_id is not None and pp_document_code is not None:
            payload_profile = lxml.etree.SubElement(cpp, cppa_ns('PayloadProfile'), id=pp_id)
            payload_part = lxml.etree.SubElement(payload_profile, cppa_ns('PayloadPart'),
                                                 minOccurs='1', maxOccurs='1')
            partname= lxml.etree.SubElement(payload_part, cppa_ns('PartName'))
            partname.text = 'businessdocument'
            mimetype = lxml.etree.SubElement(payload_part, cppa_ns('MIMEContentType'))
            mimetype.text = 'application/xml'
            edigas_doctype = lxml.etree.SubElement(payload_part,
                                                   cppa_ns('Property'),
                                                   minOccurs='1',
                                                   maxOccurs='1',
                                                   name='EDIGASDocumentType',
                                                   value=pp_document_code)

# Handling and migration of service, payload and messaging protocol profile versions

## Introduction

This repository showcases an approach of using data exchange models and
standards, profile and agreement representation formats, unification and 
matching algorithms to address configuration management and 
flexible migration to newer versions of payload schemas and messaging 
profiles.  The application domain of main interest is the European gas domain facilitated 
by the [European network of transmission system operators for gas](https://www.entsog.eu) 
but some of the underlying ideas and technologies can be used in other domains, in particular
domains facilitated by [the eDelivery Building Block](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eDelivery). 

In particular, it shows:

* Leveraging data exchange models, in particular [the Edig@s 
mapping table for AS4](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation).
* Representing service, payload and messaging capabilities using [CPPA3 profile documents](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__1473_174928413).
* Representing agreed service, payload and messaging configurations using [CPPA3 agreement documents](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__1473_174928413).
* Using [some version 3 features of CPPA3](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__19121_111695701) to keep these representations concise and easy to manage.
* Using [a defined unification algorithm](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__24555_1161199312) to automatically form agreements.  
* Handling different co-existing versions of definitions of data services and 
payloads, such as [different versions of Edig@s](https://www.edigas.org/wp-content/Downloads/edigasv6/Edigas-gas_Version_5_and_6_Comparison.pdf).
* Handling different versions of messaging protocol profiles, such as [different incompatible 
versions of ENTSOG AS4](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation).

This readme explains:

* How the gas domain models its data exchanges and how these models can 
be represented and used.
* How to represent and process profiles (capabilities and settings of one party).
* How to identity and encode different usage profile versions.
* How to represent and process agreements (agreed pairwise configurations
for to interacting parties)

Some introductory information is given about CPPA3, but this is
not a complete introduction or tutorial. For more information, reference [the
 specification at the OASIS site](http://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.pdf).

## Data exchanges in the gas market

The data exchanges used by transmission system operators for 
gas are described in ENTSOG Business Requirement Specifications and 
the [Edig@s technical specifications](https://www.edigas.org) 
developed by [EASEE-gas](https://easee-gas.eu). 

The most
recent version of Edig@s is version 6.  However, the older versions
5 and even 4 are still used by some companies.
A convenient 
and succint summary description of the exchanges in the three 
versions is provided 
in the so-called
[ENTSOG AS4 mapping table](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation). 

This table can be viewed as a model of B2B services in the gas market. 
It identifies: 

* the different types of roles that companies can perform in gas market exchanges. 
* the different process areas defined by Edig@s versions.
* Edig@s document types that can be exchanged between parties in processes.

Edig@s has an extensive system of code lists that is used in the table:

* Service process are codes (see the corresponding tab) identify codes for the three
 Edig@s versions. In the 
current version of the table there are 19 codes for three versions of Edig@s, each 
with 3 areas for Edig@s 4, 10 for Edig@s 5 and 6 for Edig@s 6. Nomination and 
matching is *A05* for 
Edig@s 5 and *A16* for Edig@s 6.
* role codes identity roles. Each version of
Edig@s has its own set of code values and definitions.  *ZSO* is system operator
in Edig@s 4 and 6 and transit system operator in Edig@s 5. 
* document type codes identity payload content types. Each version of
Edig@s has its own set of code values and definitions. e.g. *01G* is 
a nomination in Edig@S 5 and a connection point nomination in Edig@s 6.

The following is an excerpt from the service process areas:    


    Process Area Code	Process Area Value	                         EDIGAS Version
    A01	                Edigas 4.0 Sales related messages	         4
    A02	                Edigas 4.0 Infrastructure related messages   4
    A05	                Edigas 5.1 Gas Trading Processes	         5
    A06	                Edigas 5.1 Nomination and Matching Processes 5
    A16	                Edigas 6.0 Nomination & Matching	         6
    A17	                Edigas 6.0 Balancing & Settlement	         6

The following is an excerpt from the Edig@s 6 role type set:




    Party Role Code	    Party Role Value	
    ZSH	                Balance Responsible Party.	
    ZSO	                System Operator.
    ZUO	                LNG System Operator.
    ZUU	                Trader.	

The following is an excerpt from the Edig@s 6 standard document types:

    Code    Value	                                         Schema	Definition
    294     Application error and acknowledgement.           ACKNOW	Message used by an application to acknowledge reception of a message and/or to report any errors.
    01G	    Connection point nomination.                     NOMINT	Message used by a Balance Responsible Party to nominate the physical flows to a System Operator.
    02G	    Virtual trading point (VTP) OTC nomination.      NOMINT	A non-physical over the counter hub for trading in natural gas markets for a given market area.
    03G	    Virtual trading point (VTP) exchange nomination. NOMINT	A non-physical exchange hub for trading in natural gas markets for a given market area that is only used by the Clearing Responsible.
    04G	    Non-matching nomination.	                     NOMINT	A nomination to a Final Customer that has no counterpart and consequentially cannot be matched.


The table can be used to configure AS4 messaging based on the [ENTSOG
AS4 Usage Profile](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation). 
The allowed combinations of values for AS4 message headers for service, from role, to role
and the EDIGASDocumentType payload part property are specified in the
message exchange tab in the table. 




    Edigas Process Area Value	    AS4 Service	AS4 Action	                                            From Role Code Value            To  Role Code	Value	                    Part Property EDIG@S Document Type Code	
    Edig@s 6 Nomination & Matching	A16	        http://docs.oasis-open.org/ebxml-msg/as4/200902/action	ZSO	           System Operator.	ZSH	            Balance Responsible Party.	AND	
    Edig@s 6 Nomination & Matching	A16	        http://docs.oasis-open.org/ebxml-msg/as4/200902/action	ZSO	           System Operator.	ZSO	            System Operator.	        26G	
    Edig@s 6 Nomination & Matching	A16	        http://docs.oasis-open.org/ebxml-msg/as4/200902/action	ZSO	           System Operator.	ZSO	            System Operator.	        ANC	
    Edig@s 6 Nomination & Matching	A16	        http://docs.oasis-open.org/ebxml-msg/as4/200902/action	ZSO	           System Operator.	ZSO	            System Operator.	        27G	


The [service_model](service_model.py) Python 
module implements
a *ServiceModule* class that can hold the exchange model in a
convenient in-memory format. It can be instantiated from [the 
spreadsheet file containing the mapping table](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation), 
if available in [OASIS/ISO Open Document Format](https://docs.oasis-open.org/office/) file format.

## Use in EASEE-connect

The service model is already used by the [EASEE-connect collaboration 
platform](https://easee-gas.eu/easee-connect). The platform uses an
alternative JSON-based input format for its collaboration metadata. For
convenience, the module can generate this format from the sheet.
Sample output is in the [easee_connect](/easee_connect) folder

Note that the copy of the spreadsheet in this repository is an 
example snapshot. 
The latest authoritative version of the table is provided [at the
ENTSOG site](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation).  

## Profile examples

Before looking at CPPA3 profile representations of company profiles we start with an 
even simpler format.  This is both for 
expository reasons and because for ENTSOG, it is possible
to use a short-hand format that is even simpler that CPPA3 and 
just states the roles and 
services that a company can perform.  This outsources some of 
the complexities of the service model-related 
information which for
ENTSOG exchanges is documented and managed in the mapping table.

A hypothetical, non-standard, 
ad hoc example of such a
simpler format (using JSON) is used in the sample [parties.json](parties.json) file.
It shows three hypothetical companies, all of which can perform the *ZSO* 
(transmission system operator) role:

* A TSO in Member State XX that supports nomination, matching, 
settlement and balancing using Edig@s 5 only, i.e. the relevant service process areas are *A06*, *A07* and *A08*.
* A TSO in Member State YY that also supports nomination, matching, 
settlement and balancing using both Edig@s 5 and Edig@s 6, i.e. the relevant service process areas are *A06*, *A07*, *A08*, *A16* and *A17*.
* A TSO in Member State XX that supports nomination, matching, 
settlement and balancing using Edig@s 6 only, i.e. the relevant services process area codes are *A16* and *A17*.

In addition to Edig@s related services, the YY and ZZ parties also support:

* the ebMS3 built-in default test service defined [in section 5.2.2.8 of the ebMS3 Core Specification](https://docs.oasis-open.org/ebcore/ebcore-au/v1.0/cs01/ebcore-au-v1.0-cs01.html#_Toc462925653). 
* the [ebCore Certificate Update service](https://docs.oasis-open.org/ebcore/ebcore-au/v1.0/cs01/ebcore-au-v1.0-cs01.html#_Toc462925653).

## Representing capabilities in CPPA3 profile documents

The [OASIS CPPA3](http://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.pdf) 
specification supports the configuration and deployment of AS4 
messaging systems by providing machine-readable interoperable formats 
for:
 
* party's capabilities in so-called collaboration protocol profiles (or 
CPPs). 
* agreed combinations of two party profiles for bilateral data exchange (CPAs). 

These concepts are introduced and explained in [section 2.1](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__151492_666178866)  and 
other sections of the CPPA3 specification. 

CPP is a structured and well-documented format, and its latest 
version 3.0 added some features to make CPPs more easy to create 
manually using schema-aware XML editors. Alternatively and likely more 
conveniently, future versions of messaging products may 
implement CPPA3 and
allow users to edit, import or export profile information from 
internal formats. 

This repository has scripting ([run_poc.py](run_poc.py) 
and [cppa3_profile.py](cppa3_profile.py)) that uses [the sample
party data](parties.json) and the exchange information from [the mapping table](model/INT0698_210604_AS4-mapping-table_Edigas_4_7-3.ods) 
to generate CPPA3 profiles. The generated profiles are in 
[cppa3_profiles](cppa3_profiles), one XML file for each 
party.  In CPPA3 profiles:

* supported services are encoded per role and per role of an (unspecified) 
counterparty in *ServiceSpecification* structures.
* for each service, the binding specifies the data flows between the party 
and counterparty, in either direction between the two.
* for each data flow, a link is specified to a so-called payload 
profile. Following [section 2.3.6 of ENTSOG 
AS4 profile](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation),
this specifies the use of the *EDIGASDocumentType* payload part 
property and specific required value for the exchange.


## Handling AS4 profile versions

Apart from support for different versions of Edig@s, and the
data service and payload model and format definitions they provide, 
companies can differ on the messaging protocol(s) and version(s) of
any usage profiles they support.

The complexities of the details of AS4 usage profiles like ENTSOG AS4 can
be hidden by using the so-called channel profile concept introduced 
in [section 2.2.7 of CPPA3](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__19121_111695701), 
the [CPPA3 schema](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/schema/cppa3.xsd) 
and the [schema documentation](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/documentation/cppa3.html#ChannelProfile).

Today, companies in the European gas sector use [version 3.6 of the 
ENTSOG AS4 Usage Profile](https://www.entsog.eu/interoperability-and-data-exchange-nc#as4-documents-for-implementation). 
In the future, an incompatible version 4.0 may be introduced to 
modernize the security features of the profile. Companies most likely
will introduce such a new profile version at different times, meaning 
they will need to support more than one profile and initially use the newer 
version just with some counterparties but not yet with all.

Again, in the simple JSON configuration file for three hypothetical parties [(parties.json)](parties.json) 
file it is specified that: 

* TSO from XX only supports the current version 3.6.
* TSO from YY supports both 3.6 and 4.0.
* TSO from ZZ supports only 4.0.

A use of (a version of a profile of) a messaging protocol is
expressed in CPPA3 profiles by including *ChannelId* element 
that references a definition of a messaging channel. For ENTSOG AS4, 
that defined channel is an *ebMS3Channel*. 

A single CPPA3 document can have multiple such channel definitions. If more than one such
channel reference is provided for a message exchange, the referenced channels are 
alternatives, ordered by preference. This is used in the profile for
the TSO in YY, which support a v3.6 AS4 channel and a v4.0 AS4 channel.  

The scripts of this repository use [the profile module of the open source 
cppa3 library](https://pypi.org/project/cppa3/) which allows definition 
of default templates for channel profiles that can be complemented and 
overridden by specific values. This repository provides incomplete
[definitions 
for the 3.6 and (currently hypothetical) 4.0 versions](./as4_profiles/channel_defaults.xml).   

Note that profile versions may also require other configuration 
differences than version number. For example, if the hypothetical 
v4.0 switches to [the Ed448 variant of 
EdDSA](https://datatracker.ietf.org/doc/html/rfc8032#section-5.2), 
which is based on a different signing algorithm that uses a different
type of signing keys than RSA, 
the signing public key for version 4.0 will be different from the one
for version 3.6. 


## Creating CPPA3 profiles

With the minimal party configuration information in 
 [the hypothetical simple JSON configuration file (parties.json)](parties.json) 
file, it is possible to automatically generate XML party 
profile documents in CPPA3 format.  An example implementation of this 
approach is provided in [cppa3_profile.py](cppa3_profile.py), with
the following caveats: 

* the generated profiles include all document types for the supported
services and role. In practice, a company may support a smaller number.
* the generated profiles reference profile version using 
the CPPA3 *ChannelProfile* identifier feature.
* It is possible 
to encode all ENTSOG AS4 features in CPPA3, but this implementation is 
limited to some definitions of the features of these profile versions,
in particular their signature algorithm identifier and public keys.  

The outputs in [cppa3_profiles](/cppa3_profiles) are for the three sample
companies in [parties.json](parties.json).  To showcase the dependency of
algorithms on specific keys, the profile for party YY has a link from its
ENTSOG AS4 v3.6 channel to an RSA-based certificate and from 
its (hypothetical) v4.0 channel to an EdDSA-based certificate. 


## Unifying profiles into agreements and CPPA3 representations

Purely based on business process and Edig@s versions, from the party 
information it should be clear that:

* the TSO from YY may exchange inter-TSO data exchanges with both the TSOs 
from XX and ZZ.
* The TSOs from XX and ZZ cannot exchange Edig@s data exchanges as 
there is no common version that they both support. 

From the AS4 profile version support information and the 
data service constraints, it follows that
XX and YY must use 3.6 to interact, whereas YY and ZZ must use 4.0. XX and 
ZZ have no compatible messaging channel.

A main benefit of using the CPPA3 profile representation is that 
the CPPA3 specification [precisely defines a method to form a shared CPPA3 agreement 
representation](https://docs.oasis-open.org/ebcore/cppa/v3.0/cs01/cppa-v3.0-cs01.html#__RefHeading__24555_1161199312), 
which is implemented in [the open source CPPA3 package](https://pypi.org/project/cppa3/). 
Agreements can be generated using a simple call using it 
*unify.py* module.  As the naming of that module suggests, this algorithm
is similar to and inspired by the concept of unification in logic programming. 

The outputs in [cppa3_agreements](/cppa3_agreements) are for the profiles 
in [cppa3_profiles](/cppa3_profiles). They correctly reflect the 
constraints and interdependencies of the sample input data. Unused profile data (e.g. the
RSA-based certificate of YY and its v3.6 channel in [its agreement with ZZ](cppa3_agreements/21X-YY-Y-A0A0A-Y_21X-ZZ-Z-A0A0A-Z.cpa.xml)) is removed.

## Running this example

This repository contains sample data, a version of the mapping table
and code for processing them. The profiles and agreements provided here
can be (re)created using the provided Python code.
 
The [run_poc.py](run_poc.py) module calls the processing of the examples discussed
in this readme.  The only pre-requesiste is that 
you need to install the *cppa3* module and the libraries on which
it in turn depends.    

## Related work

This repository is an updated from [an earlier proof-of-concept](https://bitbucket.org/ebcore/as4_mgmt_poc/).
It adds handling of different profile versions and updates to the 2021
version of the AS4 Mapping Table.

## License

This software is licensed under [European Union Public 
Licence (EUPL) version 1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

## Support

This module is unsupported and 
provided as-is without warranty. 